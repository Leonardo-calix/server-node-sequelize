const express = require('express');
const cors = require('cors');
const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static('public'));


//app.use('/index', require('./routes/index'));
app.use('/productos', require('./routes/productos'));
app.use('/usuarios', require('./routes/user'));



app.listen(4200, () => console.log('Servidor activo'));