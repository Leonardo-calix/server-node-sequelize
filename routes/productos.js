const { Router } = require('express');
const Products = require('../models').Product;
const router = Router();

// obtener todos los productos
router.get('/', async (req, res) => {

    try {

        let productos = await Products.findAll();

        if (productos.length > 0) {
            res.json(productos);
        } else {
            res.json({ mensaje: 'No hay productos' })
        }
    } catch (error) {
        res.status(500).json(error)
    }

    /*
        Products.findAll()
            .then(res => res.json(res))
            .catch(error => res.json(error));
    */
    //res.json({ mansaje: 'Servidor listo!' });
});

// crear un nuevo priducto
router.post('/', async (req, res) => {

    try {
        let nuevoProducto = await Products.create({
            name: req.body.name,
            description: req.body.description,
            price: req.body.price
        });

        res.json(nuevoProducto);
    } catch (error) {
        res.status(500).json(error)
    }
});

// obtener un producto
router.get('/:id', async (req, res) => {

    try {

        let producto = await Products.findOne({
            where: {
                id: req.params.id
            }
        });

        if (producto) {
            res.json(producto);
        } else {
            res.json({ mensaje: 'Producto no encontrado' })
        }

    } catch (error) {
        res.status(500).json(error);
    }
});


// actualixar un producto
router.put('/:id', async (req, res) => {

    try {


        let producto = await Products.findOne({
            where: {
                id: req.params.id
            }
        });

        if (producto) {

            await producto.update({
                name: req.body.name,
                description: req.body.description,
                price: req.body.price
            });

            res.json(producto);

        } else {
            res.json({ mensaje: 'Producto no encontrado' });
        }

    } catch (error) {
        res.status(500).json(error);
    }
});


// eliminar un producto
router.delete('/:id', async (req, res) => {

    try {

        let producto = await Products.findOne({
            where: {
                id: req.params.id
            }
        });

        if (producto) {

            let result = await producto.destroy();

            res.json({ mensaje: 'Producto eliminado', result });

        } else {
            res.json({ mensaje: 'Producto no encontrado' });
        }

    } catch (error) {
        res.status(500).json(error);
    }
});



module.exports = router;