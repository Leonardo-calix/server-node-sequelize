const { Router } = require('express');
const Users = require('../models').User;
const Emails = require('../models').Email;
const { Op } = require("sequelize");
const router = Router();

// obtener todos los productos
router.get('/', async (req, res) => {

    try {

        let usuarios = await Users.findAll({
            include: [{
                model: Emails,
                where: {
                    //dominio: 'Gmail'
                    [Op.or]: [{ dominio: 'Gmail' }, { dominio: 'Apple' }],
                }
            }]
        });

        if (usuarios.length > 0) {
            res.json(usuarios);
        } else {
            res.json({ mensaje: 'No hay usuario' })
        }
    } catch (error) {
        res.json({ mensaje: JSON.stringify(error) })
        console.log(error)
    }

});

module.exports = router;