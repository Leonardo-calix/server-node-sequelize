const { Router } = require('express');
const router = Router();

router.get('/', (req, res) => {
    res.json({ mansaje: 'Servidor listo!' });
});

module.exports = router;